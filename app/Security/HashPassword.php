<?php
namespace IK_Authentication\Security;

/**
 * Class Hash. Used to hash the password.
 */
class HashPassword
{
    protected $password;

    /**
     * @param $password, password to hash.
     * @return null|string
     */
    public function __construct($password)
    {
        $password = Validate::password($password);
        if($password) {
            $this->password = password_hash($password, PASSWORD_DEFAULT);
        }

        return null;
    }

    public function get()
    {
        return $this->password;
    }
}