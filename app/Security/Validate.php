<?php

namespace IK_Authentication\Security;

use IK_Authentication\Views\Message;

/**
 * Class Validate. Used to validate and filter values.
 */
class Validate
{

    /**
     * Validates the username
     *
     * @param string $username
     * @return string
     */
    public static function username($username)
    {
        if (!preg_match("/^[A-Za-z][A-Za-z0-9_]{3,25}$/", $username)) {
            Message::error('Incorrect username length. Username must be at least 4 characters long');
            return false;
        }

        return $username;
    }

    /**
     * Validates the email
     *
     * @param string $email
     * @return string
     */
    public static function email($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return filter_var($email, FILTER_SANITIZE_EMAIL);
        }

        Message::error('Email is not valid');
        return false;
    }

    /**
     * Validates the password
     *
     * @param string $password
     * @return string
     */
    public static function password($password)
    {
        $lowercase = preg_match('@[a-z]@', $password);
        $number = preg_match('@[0-9]@', $password);

        if (!$lowercase) {
            Message::error('Wrong password. Password should contain a lower case character');
            return false;
        }

        if (!$number) {
            Message::error('Wrong password. Password should contain a number');
            return false;
        }

        if (strlen($password) < 4) {
            Message::error('Wrong password. Password must be at least 4 characters long');
            return false;
        }
        return $password;
    }

    /**
     * Escapes html characters.
     *
     * @param $data , data to escape
     * @return string
     */
    public static function e($data)
    {
        return htmlspecialchars($data, 'ENT_QUOTES', 'UTF-8');
    }

}
