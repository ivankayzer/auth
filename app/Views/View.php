<?php
namespace IK_Authentication\Views;
/**
 * Class View. Used to render html pages.
 */
class View
{
    private $data = [];
    private $render = false;

    /**
     * View constructor. Assigns file path to the $render.
     *
     * @param string $name Name of the view.
     * @throws \Exception if file not found.
     */
    public function __construct($name)
    {
        $file = AUTH_ROOT . '/views/' . strtolower($name) . '.view.php';
        if(file_exists($file)){
            $this->render = $file;
        }
        else{
            throw new \Exception('View file not found.');
        }
    }

    /**
     * Creates an array with data to pass into the view.
     *
     * @param string $variable Name of variable to assign.
     * @param mixed $value Value of the variable.
     */
    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }

    /**
     * Passes the data to the view and shows the view.
     */
    public function __destruct()
    {
        extract($this->data);
        include($this->render);
    }
}