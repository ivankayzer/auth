<?php
namespace IK_Authentication\Views;
use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Redirect;

/**
 * Class Message. Used to show the messages.
 */
class Message
{

    /**
     * Puts the message with desired type to the session.
     * Recommended types are error, success, warning, info.
     *
     * @param string $type Type of message
     * @param string $message Message
     *
     *
     */
    public static function __callStatic($type, $message)
    {
        if(!isset($_SESSION['messages'])){
            $_SESSION['messages'] = array();
        }

        $_SESSION['messages'][$type] = $message[0];
    }

    /**
     * Displays a message from session container.
     * If type is empty, displays all messages
     *
     * @param string $type
     */
    public static function display($type = 'any')
    {
        if(isset($_SESSION['messages'])) {
            if ($type != 'any') {
                if (isset($_SESSION['messages'][$type])) {
                    echo self::print($type, $_SESSION['messages'][$type]);
                    self::clear($type);
                }
            } else {
                foreach ($_SESSION['messages'] as $kind => $msg) {
                    echo self::print($kind, $msg);
                    unset($_SESSION['messages'][$kind]);
                }
            }
        }
    }

    /**
     * Deletes the message from the session.
     *
     * @param string $type
     */
    public static function clear($type)
    {
        unset($_SESSION['messages'][$type]);
    }

    /**
     * Add styling and tags to the message.
     *
     * @param string $type
     * @param string $message
     * @return string with styling and message.
     */
    public static function print($type, $message)
    {

        // Bootstrap types

        $types = array(
            'error' => 'danger',
            'success' => 'success',
            'warning' => 'warning',
            'info' => 'info'
        );
        if(!array_key_exists($type, $types)){
            $type = 'warning';
        }
        return "<div class='alert alert-{$types[$type]}'>$message</div>";
    }

    /**
     * Generates forbidden message and redirects to homepage. Used when csrf tokens do not match
     * @param string $action to show what is forbidden :)
     */
    public static function forbidden($action = '')
    {
        Message::error('Forbidden');
        $log = new \Monolog\Logger('auth');
        $log->pushHandler(new \Monolog\Handler\StreamHandler(AUTH_ROOT . '/app/log.txt', \Monolog\Logger::WARNING));
        $log->warning("$action warning. CSRF TOKENS DO NOT MATCH!");
        Redirect::redirect();
    }

    /**
     * @return bool, true if there are some errors in the session
     */
    public static function hasErrors(){
        return (bool) isset($_SESSION['messages']['error']);
    }

}