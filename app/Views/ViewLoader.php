<?php
namespace IK_Authentication\Views;

use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Captcha;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Session;
use IK_Authentication\Core\Tokenizer;
use IK_Authentication\Providers\Facebook;
use IK_Authentication\Providers\Google;
use IK_Authentication\Providers\Qr;
use IK_Authentication\Providers\Sms;
use IK_Authentication\Providers\Twitter;
use IK_Authentication\Core\Config;

class ViewLoader
{
    public function index()
    {
        Redirect::redirectAuthorized();
        $fb = new Facebook();
        $g = new Google;
        $tw = new Twitter();
        $sms = new Sms();
        // $qr = new Qr();
        $view = new View('index');
        $view->assign('captcha', Captcha::generate());
        $view->assign('type', Config::getAuthType());
        if(Config::get('project.enableSocials')){
            $view->assign('facebook', $fb->generateAccessButton());
            $view->assign('google', $g->generateAccessButton());
            $view->assign('twitter', $tw->generateAccessButton());
        }

        $view->assign('sms', $sms->generateAccessButton());
        // $view->assign('qr', $qr->generateAccessButton());
        return $view;
    }

    public function dashboard()
    {
        Auth::only();
        Tokenizer::generateCsrfToken();

        (new Session)->createProfileOnFirstLogin();

        return new View('dashboard');
    }

    public function reset()
    {
        return new View('reset');
    }

    public function resetSubmit($hash)
    {
        $view = new View('resetsubmit');
        $view->assign('hash', $hash);
        return $view;
    }

    public function testView()
    {
        return new View('test');
    }

    public function sms()
    {
        return new View('sms');
    }

    public function smsCheck()
    {
        return new View('smsCheck');
    }

    public function qr()
    {
        return new View('qr');
    }
}