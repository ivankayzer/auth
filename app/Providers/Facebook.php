<?php
namespace IK_Authentication\Providers;

use Facebook\Facebook as FacebookAPI;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Models\User;
use IK_Authentication\Views\Message;

/**
 * Class Facebook. Used to authorize users using Facebook through Facebook PHP SDK.
 * You should make app public before accessing user's email.
 * developers.facebook.com/apps/
 */
class Facebook implements AuthenticationProvider
{
    protected $api;
    protected $loginUrl;
    protected $token;

    protected $name;
    protected $email;
    protected $id;

    /**
     * Facebook constructor.
     * Creates SDK instance and generates URL for the link to log in.
     */
    public function __construct()
    {
        $projUrl = Config::get('project.url');
        $this->api = new FacebookAPI([
            'app_id' => Config::get('facebook.app_id'),
            'app_secret' => Config::get('facebook.app_secret'),
            'default_graph_version' => Config::get('facebook.default_graph_version')
        ]);
        $helper = $this->api->getRedirectLoginHelper();
        $permissions = ['email'];
        $this->loginUrl = $helper->getLoginUrl("{$projUrl}/facebook/", $permissions);
    }


    /**
     * Method called after authorizing this App. Retrieves the access token.
     *
     */
    public function callback()
    {
        $helper = $this->api->getRedirectLoginHelper();
        try{
            $accessToken = $helper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e){
            Message::error('Graph returned an error: ' . $e->getMessage());
        } catch (Facebook\Exceptions\FacebookSDKException $e){
            Message::error('Facebook SDK returned an error: ' . $e->getMessage());
        }

        if(isset($accessToken)){
            $this->token = (string) $accessToken;
            $_SESSION['facebook_access_token'] = $this->token;
            $this->getUserFullName();
        }
    }

    /**
     * Access the user's data using token.
     * Returns the User's full name if set.
     *
     * @return null|string user's name
     */
    public function getUserFullName()
    {
        $this->api->setDefaultAccessToken($this->token);
        try {
            $response = $this->api->get('/me?fields=id,name,email', "{$this->token}");
            $userData = $response->getGraphUser();
        } catch (Facebook\Exceptions\FacebookResponseException $e){
            Message::error('Graph returned an error: ' . $e->getMessage());
        } catch (Facebook\Exceptions\FacebookSDKException $e){
            Message::error('Facebook SDK returned an error: ' . $e->getMessage());
        }

        $this->name = $userData->getName();
        $this->email = $userData->getEmail();
        $this->id = $userData->getId();

        $data = [
            'username' => $this->name,
            'email' => $this->email,
            'id' => $this->id,
            'type' => 'facebook'
        ];

        (new User)->processSocialLogin($data);
        return Redirect::redirect('dashboard');
    }

    /**
     * Get User's email
     *
     * @return null|string user's email
     */
    public function getUserEmail()
    {
        if(empty($this->email)){
            $this->getUserFullName();
        }
        return $this->email;
    }

    /**
     * Generate pretty link-button
     *
     * @return string to embed to HTML
     */
    public function generateAccessButton()
    {
       return '<a href="' . $this->loginUrl . '" class="btn btn-social-icon btn-facebook">
                        <span class="fa fa-facebook"></span>
                    </a>';
    }
}