<?php

namespace IK_Authentication\Providers;

use IK_Authentication\Core\Redirect;
use IK_Authentication\Services\SmsService as SmsApi;
use IK_Authentication\Core\Auth;


class Sms implements AuthenticationProvider
{
    protected $url;
    protected $api;

    public function __construct()
    {
        $this->url = '/auth/sms';
        $this->api = new SmsApi();
    }

    public function callback()
    {
        $this->api->send($_POST['phone'], '');
        Redirect::redirect('/smsCheck?phone=' . $_POST['phone']);
    }

    public function generateAccessButton()
    {
        return '<a href="' . $this->url . '" class="btn btn-social-icon btn-sms">
                        <span class="fa fa-commenting-o"></span>
                    </a>';
    }

    public function getUserFullName()
    {
        // TODO: Implement getUserFullName() method.
    }
}