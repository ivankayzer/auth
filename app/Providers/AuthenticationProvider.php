<?php
namespace IK_Authentication\Providers;

interface AuthenticationProvider
{
    public function __construct();
    public function callback();
    public function generateAccessButton();
    public function getUserFullName();
}