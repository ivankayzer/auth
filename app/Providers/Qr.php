<?php

namespace IK_Authentication\Providers;

use IK_Authentication\Services\QrService as QrService;

class Qr implements AuthenticationProvider
{

    protected $url;

    public function __construct()
    {
        $this->url = 'qr';
    }

    public function callback()
    {
        echo "<img src='" . (new QrService)->generate() . "'/>";
    }

    public function generateAccessButton()
    {
        return '<a href="' . $this->url . '" class="btn btn-social-icon btn-qr">
                        <span class="fa fa-qrcode"></span>
                    </a>';
    }

    public function getUserFullName()
    {
        // TODO: Implement getUserFullName() method.
    }
}