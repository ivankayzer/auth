<?php
namespace IK_Authentication\Providers;

use Google_Client as GoogleAPI;
use Google_Service_Oauth2 as oAuthAPI;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Models\User;

/**
 * Class Google. Used to authorize users using Google+ API.
 * console.developers.google.com
 */
class Google implements AuthenticationProvider
{
    protected $api;
    protected $OAuthService;
    protected $userData;

    /**
     * Google constructor.
     * Create an instances of GoogleClient and GoogleServiceOAuth2 .
     */
    public function __construct()
    {
        $this->api = new GoogleAPI;

        $this->api->setApplicationName('Auth');
        $this->api->setClientId(Config::get('google.client_id'));
        $this->api->setClientSecret(Config::get('google.client_secret'));
        $this->api->setRedirectUri(Config::get('google.redirect_url'));
        $this->api->setDeveloperKey(Config::get('google.api_key'));
        $this->api->addScope("https://www.googleapis.com/auth/userinfo.email");

        $this->OAuthService = new oAuthAPI($this->api);
    }

    /**
     * Method called after authorizing this App. Retrieves the access token.
     */
    public function callback()
    {
        if (isset($_GET['code'])) {
            $this->api->authenticate($_GET['code']);
            $_SESSION['access_token'] = $this->api->getAccessToken();
            header('Location: ' . filter_var(Config::get('google.redirect_url'), FILTER_SANITIZE_URL));
        }

        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->api->setAccessToken($_SESSION['access_token']);
        }

        if($this->api->getAccessToken()){
            $this->userData = $this->OAuthService->userinfo->get();
            $_SESSION['access_token'] = $this->api->getAccessToken();
        }
        else {
            $this->authUrl = $this->api->createAuthUrl();
        }

        $this->userData->username = $this->userData->name;
        $this->userData->type = 'google';
        (new User)->processSocialLogin((array) $this->userData);

        return Redirect::redirect('dashboard');
    }

    /**
     * Generate pretty link-button
     *
     * @return string to embed to HTML
     */
    public function generateAccessButton()
    {
        return '<a href="' . $this->api->createAuthUrl() . '" class="btn btn-social-icon btn-google">
                        <span class="fa fa-google"></span>
                    </a>';
    }

    /**
     * @return bool
     */
    public function userDataExists()
    {
        return (bool) !empty($this->userData);
    }

    /**
     * Get User's full name
     *
     * @return null|string user's full name
     */
    public function getUserFullName()
    {
        if($this->userDataExists()) return $this->userData->name;
        return null;
    }

    /**
     * Get User's email
     *
     * @return null|string user's email
     */
    public function getUserEmail()
    {
        if($this->userDataExists()) return $this->userData->email;
        return null;
    }
}