<?php
namespace IK_Authentication\Providers;
//https://apps.twitter.com/
//callback url
// privacy policy URL & Terms of Service URL
use Abraham\TwitterOAuth\TwitterOAuth as TwitterAPI;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Models\User;

class Twitter implements AuthenticationProvider
{
    public $connect;
    public $url;

    public function __construct()
    {
        if (!isset($_REQUEST['oauth_token'])){
            $this->connect = new TwitterAPI(Config::get('twitter.consumer_key'), Config::get('twitter.consumer_secret'));
            $requestToken = $this->connect->oauth('oauth/request_token', [
                'oauth_callback' => Config::get('twitter.url_callback')
            ]);

            if ($this->connect->getLastHttpCode() != 200) {
                throw new \Exception('There was a problem performing this request');
            }

            $_SESSION['oauth_token'] = $requestToken['oauth_token'];
            $_SESSION['oauth_token_secret'] = $requestToken['oauth_token_secret'];

            $this->url = $this->connect->url('oauth/authorize', [
                'oauth_token' => $requestToken['oauth_token']
            ]);
        }
    }

    public function callback()
    {
        $request_token = [];
        $request_token['oauth_token'] = $_SESSION['oauth_token'];
        $request_token['oauth_token_secret'] = $_SESSION['oauth_token_secret'];

        if (isset($_REQUEST['oauth_token']) && $request_token['oauth_token'] !== $_REQUEST['oauth_token']) {
            throw new \Exception('Something is wrong');
        }

        $this->dropConnection();

        $this->connect = new TwitterAPI(Config::get('twitter.consumer_key'), Config::get('twitter.consumer_secret'), $request_token['oauth_token'], $request_token['oauth_token_secret']);
        $access_token = $this->connect->oauth("oauth/access_token", ["oauth_verifier" => $_REQUEST['oauth_verifier']]);

        $_SESSION['access_token'] = $access_token;

        $this->getUserFullName();
    }

    //Test profile

    public function getUserFullName()
    {
        $this->dropConnection();
        $access_token = $_SESSION['access_token'];
        $this->connect = new TwitterAPI(Config::get('twitter.consumer_key'), Config::get('twitter.consumer_secret'), $access_token['oauth_token'], $access_token['oauth_token_secret']);
        $user = $this->connect->get("account/verify_credentials");

        $user->username = $user->screen_name;
        $user->type = 'twitter';
        $user->email = '';

        (new User)->processSocialLogin((array) $user);

        return Redirect::redirect('dashboard');
    }

    public function generateAccessButton()
    {
        return '<a href="' . $this->url . '" class="btn btn-social-icon btn-twitter">
                        <span class="fa fa-twitter"></span>
                    </a>';
    }

    public function dropConnection()
    {
        $this->connect = null;
    }
}