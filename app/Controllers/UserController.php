<?php

namespace IK_Authentication\Controllers;


use IK_Authentication\Core\Captcha;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Mail;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Session;
use IK_Authentication\Core\Tokenizer;
use IK_Authentication\Security\HashPassword;
use IK_Authentication\Services\DatabaseService;
use IK_Authentication\Views\Message;
use IK_Authentication\Views\View;

class UserController extends Controller
{
    protected $username;
    protected $password;
    protected $email;
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }

    /**
     * Register a user.
     * Validates the username, email, password passed by controller.
     * Executes the db query, removes the csrf-token from the session and redirects to homepage with the message.
     * @param array $regData
     * @return null
     * @throws \Exception
     * @throws \phpmailerException
     */
    public function register(array $regData = [])
    {
        $this->beforeRequest();
        $this->checkCaptcha($regData['captcha']);

        $this->username = $regData['username'];
        $this->email = $regData['email'];
        $this->password = (new HashPassword($regData['password']))->get();

        if (!self::validate('username', 'email')) {
            return Redirect::redirect();
        }

        if ($this->password === null) {
            return Redirect::redirect();
        }

        if ($this->database->getUserProfileByEmail($this->email)) {
            Message::error('User with this email already exists');
            return Redirect::redirect();
        }

        if ($this->database->getUserProfileByUsername($this->username)) {
            Message::error('User with this username already exists');
            return Redirect::redirect();
        }

        $this->database->registerUser([
            ':username' => $this->username,
            ':email' => $this->email,
            ':password' => $this->password
        ]);

        $userData = $this->database->getUserProfileByUsername($this->username);

        $mail = new Mail;
        $mail->sendWelcomeEmail([$this->email => $this->username]);
        if (Config::get('project.requireActivation') && $userData) {
            $rnd = Tokenizer::generateRandomToken();

            $this->database->saveEmailToken([
                ':user_id' => $userData->id,
                ':ver_hash' => $rnd
            ]);

            $mail->sendActivationEmail([$this->email => $this->username], $rnd);
        }

        if (Message::hasErrors()) {
            return Redirect::redirect();
        }

        Message::success('Congratulations! You have successfully registered!');
        return Redirect::redirect();
    }

    /**
     * Activate user's account
     * @param string $code Verification code
     */
    public function activate()
    {
        $code = $_GET['code'];
        $userId = $this->database->getUserIdFromEmailToken($code);

        if ($userId === false) {
            Message::error('No user found');
            Redirect::redirect();
        }

        $user = $this->database->getUserProfileById($userId->user_id);

        $this->database->activateUser($userId->user_id);
        $this->database->removeEmailToken($code);

        Session::setUser($user);
        return Redirect::redirect();
    }

    public function upgrade($email = '', $password = '')
    {
        if (Session::getUser()->email) {
            $email = Session::getUser()->email;
        }

        if (!Session::check() || !Session::getUser()) {
            return Redirect::redirect();
        }

        $view = new View('upgrade');
        if (!$email && empty(Session::getUser()->email)) {
            $view->assign('needEmail', true);
        }
        if (!$email || !$password)
            return $view;

        $this->password = (new HashPassword($password))->get();
        $this->email = $email;
        $this->username = str_replace(" ", '', Session::getUser()->username) . Session::getUser()->id;

        if (!self::validate('username', 'email')) {
            return Redirect::redirect();
        }

        $this->database->upgradeAccount($this->email, $this->password);

        $user = $this->database->getUserProfileByEmail($this->email);

        Session::setUser($user);

        if (Message::hasErrors()) {
            return Redirect::redirect();
        }

        Message::success('Congratulations! You have successfully registered!');
        return Redirect::redirect();
    }

    protected function checkCaptcha($captcha)
    {
        if (!Captcha::check($captcha)) {
            Message::error('Wrong captcha');
            Redirect::redirect();
        }
    }
}