<?php

namespace IK_Authentication\Controllers;

use IK_Authentication\Core\Mail;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Tokenizer;
use IK_Authentication\Security\HashPassword;
use IK_Authentication\Services\DatabaseService;
use IK_Authentication\Views\Message;

class ResetController extends Controller
{
    protected $username;
    protected $password;
    protected $email;
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }
    /**
     * Create a hash to reset a password, sends this hash via email and puts it into db.
     */
    public function createResetHash($username)
    {
        $this->beforeRequest();

        $this->username = $username;

        if (self::validate('username')) {
            $u = $this->database->getUserProfileByUsername($this->username);

            if ($u === false) {
                Message::error('No user found with this username');
                return Redirect::redirect();
            }

            $rn = Tokenizer::generateRandomToken();
            $mail = new Mail;
            $mail->sendPasswordReset([$u->email => $this->username], $rn);

            if (Message::hasErrors()) {
                Redirect::redirect();
            }

            $this->database->savePasswordResetToken([
                ':user_id' => $u->id,
                ':reset_hash' => $rn
            ]);

            Message::success('Check your email to reset the password');
            Redirect::redirect();
        }
    }

    /**
     * TODO arguments array. Message on successful password reset.
     * Resets the user password.
     *
     * @param $hash . Reset hash from email
     * @param $password
     * @param $confirmation
     */
    public function resetPassword($hash, $password, $confirmation)
    {
        if (Tokenizer::checkToken()) {
            $user = $this->database->getUserIdFromPasswordResetToken($hash);
            if ($user != false && $password === $confirmation) {
                $hashedPassword = (new HashPassword($password))->get();

                if ($hashedPassword === null) {
                    return Redirect::redirect();
                }

                $this->database->updateUserPassword([
                    ':pass' => $hashedPassword,
                    ':id' => $user->user_id
                ]);

                Message::success('Your password has been updated!');
            } else {
                Message::error('Wrong password reset code');
            }
        }

        return Redirect::redirect();
    }

}