<?php

namespace IK_Authentication\Controllers;


use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Session;
use IK_Authentication\Services\DatabaseService;
use IK_Authentication\Views\Message;
use IK_Authentication\Views\View;

class ProfileController extends Controller
{
    protected $username;
    protected $password;
    protected $email;
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }

    /**
     * Upload user avatar to app/storage/avatars/
     *
     * @return string. Avatar file name
     */
    public function uploadAvatar()
    {
        $fileName = '';

        if ($_FILES['avatar']['size'] > 0) {
            $targetDir = "storage/avatars/";
            $fileName = basename($_FILES['avatar']['name']);
            $uploaded = $targetDir . $fileName;
            $imageFileType = pathinfo($uploaded, PATHINFO_EXTENSION);
            if (!getimagesize($_FILES["avatar"]["tmp_name"])) {
                Message::error('File is not an image!');
                Redirect::redirect('dashboard');
            }
            if ($_FILES["avatar"]["size"] > 3000000) { // 3 mb
                echo "Sorry, your file is too large.";
            }
            if (file_exists($uploaded)) {
                $fileName = explode('.', basename($_FILES['avatar']['name']));
                $fileName[0] = bin2hex(random_bytes(10));
                $fileName = implode('.', $fileName);
            }

            $targetFile = $targetDir . trim($fileName);
            if (!move_uploaded_file($_FILES["avatar"]["tmp_name"], $targetFile)) {
                Message::error("Sorry, there was an error uploading your file.");
                Redirect::redirect('dashboard');
            }
        }

        return $fileName;
    }

    /**
     * @param array $profileData
     */
    public function updateProfile(array $profileData)
    {
        $originalProfile = Session::getProfile();
        if (!isset($profileData['sex']))
            $profileData['sex'] = $originalProfile->sex;

        if ($_FILES['avatar']['size'] == 0) {
            $profileData['avatar'] = $originalProfile->avatar;
        } else {
            $profileData['avatar'] = $this->uploadAvatar();
        }

        $profileData = (object)$profileData;
        if ($profileData !== $originalProfile) {
            $this->database->updateProfile([
                ':firstName' => $profileData->firstName,
                ':lastName' => $profileData->lastName,
                ':sex' => $profileData->sex,
                ':country' => $profileData->country,
                ':avatar' => $profileData->avatar,
                ':id' => Session::getUser()->id
            ]);
            Session::setProfile((array)$profileData);
            return Redirect::redirect();
        }
    }

    /**
     * Put user profile data to the table
     * @param array $data
     * @param string $avatar
     */
    public function createProfile(array $data = [], $avatar = '')
    {
        $fileName = $this->uploadAvatar();

        if (!empty($data['sex'])) {
            $data['sex'] = ($data['sex'] == 'Male') ? 0 : 1;
        }

        if (!empty($data) || $_FILES['avatar']['size'] > 0) {
            $this->database->createProfile([
                ':user_id' => Session::getUser()->id,
                ':firstName' => $data['firstName'],
                ':lastName' => $data['lastName'],
                ':sex' => $data['sex'],
                ':country' => $data['country'],
                ':avatar' => $fileName
            ]);

            $data['avatar'] = $fileName;
            Session::setProfile($data);
        }

        (new Session)->skipProfile();
    }

    public function recreateProfile()
    {
        return new View('profile');
    }
}