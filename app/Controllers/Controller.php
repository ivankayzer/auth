<?php

namespace IK_Authentication\Controllers;

use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Tokenizer;
use IK_Authentication\Security\Validate;
use IK_Authentication\Views\Message;

abstract class Controller
{
    public function __destruct()
    {
        $this->afterRequest();
    }

    public function beforeRequest()
    {
        if (!Tokenizer::checkToken()) {
            Message::forbidden('Token is incorrect');
            Redirect::redirect();
            exit;
        }
    }

    public function afterRequest()
    {
        Tokenizer::removeToken();

        foreach ($_POST as $var) {
            unset($var);
        }

        foreach ($_GET as $var) {
            unset($var);
        }
    }

    /**
     * Validate using Validate class.
     *
     * @return bool indicating the status of validation.
     */
    public function validate()
    {
        foreach (func_get_args() as $arg) {
            $this->$arg = Validate::$arg($this->$arg);
            if ($this->$arg == false) return false;
        }
        return true;
    }
}