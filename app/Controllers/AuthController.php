<?php
namespace IK_Authentication\Controllers;

use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Session;
use IK_Authentication\Core\Tokenizer;
use IK_Authentication\Services\DatabaseService;
use IK_Authentication\Views\Message;
use IK_Authentication\Views\View;

class AuthController extends Controller
{
    protected $username;
    protected $password;
    protected $email;
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }

    /**
     * Let the user in.
     * @string $remember
     * @throws \Exception
     */
    public function login(array $loginData = [])
    {
        $this->beforeRequest();
        $type = Config::getAuthType();
        $this->$type = $loginData['id'];
        $this->password = $loginData['password'];

        if (!$this->validate($type, 'password')) {
            return Redirect::redirect();
        }

        try {
            switch ($type) {
                case 'username':
                    $userData = $this->database->getUserProfileByUsername($this->$type);
                    break;
                case 'email':
                    $userData = $this->database->getUserProfileByEmail($this->$type);
                    break;
            }

            if ($userData === false || !password_verify($this->password, $userData->password)) {
                Message::error('Wrong username or password');
                return Redirect::redirect();
            }

            if (Config::get('project.requireActivation') && !$userData->activated) {
                Message::error('You should activate account');
                return Redirect::redirect();
            }

            Session::setUser($userData);

            $profile = $this->database->getUserProfileByUserId($userData->id);

            if ($profile != false) {
                Session::setProfile((array)$profile);
            }

            if ($loginData && $loginData['remember'] == 'on') {
                $cookie = Tokenizer::generateRandomToken();
                $d = new \DateTime();
                $d->modify('+7 days');
                $d = $d->format('Y-m-d H:i:s');
                $this->database->saveSessionToken([
                    ':user_id' => $userData->id,
                    ':token' => $cookie,
                    ':expires' => $d
                ]);
                setcookie('auth_ses', $cookie, time() + 7 * 24 * 3600);
            }

            return Redirect::redirect('dashboard');
        } catch (\PDOException $e) {
            return Redirect::redirect();
        }

        return Redirect::redirect();
    }

    /**
     * Destroy cookies and session
     */
    public function logout()
    {
        $this->beforeRequest();
        unset($_COOKIE['auth_ses']);
        setcookie('auth_ses', '', time() - 3600);
        Session::destroy();
        return Redirect::redirect();
    }

    public function adminArea()
    {
        if (!Session::isAdmin()) {
            Message::error('Forbidden');
            return Redirect::redirect();
        }

        $view = new View('admin');
        $view->assign('users', $this->database->getUsersWithProfiles());
        return $view;
    }
}