<?php

namespace IK_Authentication\Models;

use IK_Authentication\Core\Captcha;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Mail;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Session;
use IK_Authentication\Core\Tokenizer;
use IK_Authentication\Security\HashPassword;
use IK_Authentication\Security\Validate;
use IK_Authentication\Services\DatabaseService;
use IK_Authentication\Views\Message;
use IK_Authentication\Views\View;

/**
 * Class User. Represents the user entity and handles registration, login, logout, etc...
 */
class User
{

    protected $username;
    protected $password;
    protected $email;
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }

    /**
     * Validate using Validate class.
     *
     * @return bool indicating the status of validation.
     */
    public function validate()
    {
        foreach (func_get_args() as $arg) {
            $this->$arg = Validate::$arg($this->$arg);
            if ($this->$arg == false) return false;
        }
        return true;
    }

    public function processSocialLogin(array $userData = [])
    {
        if (empty($userData['id']) || $userData['id'] === null) {
            Message::error('Something went wrong. Try again later');
            Redirect::redirect();
        }

        $userData['id'] = substr($userData['id'], strlen($userData['id']) - 6, strlen($userData['id']));

        $user = $this->database->getUserProfileByEmail($userData['email']);

        if ($user === false) {
            $this->database->registerSocial($userData);
        }

        if ($user) {
            Session::setUser((object)$user);

            $profile = $this->database->getUserProfileByUserId($user->id);

            if ($profile != false) {
                return Session::setProfile((array)$profile);
            }

            return Redirect::redirect();
        }

        $user = $this->database->getSocialData($userData['id']);

        if ($user === false) {
            $this->database->saveSocialData([
                ':id' => $userData['id'],
                ':type' => $userData['type'],
                ':name' => $userData['username'],
                ':email' => $userData['email']
            ]);
        }

        $userData['activated'] = 1;
        $userData['role'] = 1;
        $userData['profile'] = 1;

        Session::setUser((object)$userData);
    }
}