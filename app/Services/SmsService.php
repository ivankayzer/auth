<?php

namespace IK_Authentication\Services;

use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Session;
use IK_Authentication\Core\Tokenizer;
use IK_Authentication\Views\Message;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use SMSApi\Client;
use SMSApi\Api\SmsFactory;
use SMSApi\Exception\SmsapiException;

class SmsService
{
    protected $api;
    protected $database;

    public function init()
    {
        $client = new Client(Config::get('smsapi.email'));
        $client->setPasswordRaw(Config::get('smsapi.password'));
        $this->api = new SmsFactory;
        $this->api->setClient($client);
    }

    public function send($number)
    {
        $this->database = new DatabaseService();
        $this->init();
        try {
            $actionSend = $this->api->actionSend();
            $actionSend->setTo($number);
            $code = Tokenizer::generateToken(5);
            $actionSend->setText('Your authentication code: ' . $code);

            $actionSend->execute();

            $this->database->saveSmsToken($number, $code);
//
//            foreach ($response->getList() as $status) {
//                echo $status->getNumber() . ' ' . $status->getPoints() . ' ' . $status->getStatus();
//            }
        } catch (SmsapiException $exception) {
            $log = new Logger('sms');
            $log->pushHandler(new StreamHandler(AUTH_ROOT . '/app/log.txt', Logger::WARNING));
            $log->warning($exception->getMessage());
        }
    }

    public function check()
    {
        $this->database = new DatabaseService();

        if ($this->database->smsTokenExists($_POST['code'])) {
            $this->database->invalidateSmsToken($_POST['code']);
            $user = $this->database->getUserProfileByUsername($_POST['number']);

            if (!$user) {
                $this->database->registerUser([
                    ':username' => $_POST['number'],
                    ':email' => $_POST['number'],
                    ':password' => $_POST['number']
                ]);
                $user = $this->database->getUserProfileByUsername($_POST['number']);
            }

            $profile = $this->database->getUserProfileByUserId($user->id);

            Session::setUser($user);
            Session::setProfile((array) $profile);

            Redirect::redirect();
        } else {
            Message::error('Sms code is not valid');
            Redirect::redirect('/auth/smsCheck?phone=' . $_POST['number']);
        }
    }
}