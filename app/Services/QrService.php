<?php

namespace IK_Authentication\Services;

use Endroid\QrCode\QrCode;
use IK_Authentication\Core\Auth;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Redirect;
use IK_Authentication\Core\Session;
use IK_Authentication\Core\Tokenizer;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class QrService
{
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }

    public function generate()
    {
        $token = Tokenizer::generateRandomToken();
        $url = Config::get('project.url') . '/qr/verify/?token=' . $token;
        $qrCode = new QrCode($url);
        $log = new Logger('qr');
        $log->pushHandler(new StreamHandler(AUTH_ROOT . '/app/log.txt', Logger::WARNING));
        $log->warning($url);
        try {
            $this->database->saveQrToken($token);
            return $qrCode->writeDataUri();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function check()
    {
        if ($this->database->qrTokenExists($_GET['token'])) {
            $this->database->verifyQrToken($_GET['token']);
            $user = $this->database->getUserProfileByUsername($_SERVER['REMOTE_ADDR']);

            if (!$user) {
                $this->database->registerUser([
                    ':username' => $_SERVER['REMOTE_ADDR'],
                    ':email' => $_SERVER['REMOTE_ADDR'],
                    ':password' => $_SERVER['REMOTE_ADDR']
                ]);
                $user = $this->database->getUserProfileByUsername($_SERVER['REMOTE_ADDR']);
            }

            $profile = $this->database->getUserProfileByUserId($user->id);

            Session::setUser($user);
            Session::setProfile($profile);
        }
    }

}