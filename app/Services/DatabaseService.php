<?php

namespace IK_Authentication\Services;


use IK_Authentication\Core\Database;
use IK_Authentication\Core\Session;
use IK_Authentication\Views\Message;

class DatabaseService
{
    protected function prepare($statement)
    {
        $database = Database::getFactory()->getConnection();

        return $database->prepare($statement);
    }

    protected function execute($statement, $parameters)
    {
        $stmt = $this->prepare($statement);

        try {
            $stmt->execute($parameters);
        } catch (\Exception $e) {
            Message::warning($e->getMessage());
        }
    }

    protected function executeAndFetch($statement, $parameters)
    {
        $stmt = $this->prepare($statement);

        try {
            $stmt->execute($parameters);
        } catch (\Exception $e) {
            Message::warning($e->getMessage());
        }

        return $stmt->fetch();
    }

    protected function executeAndFetchAll($statement, $parameters)
    {
        $stmt = $this->prepare($statement);

        try {
            $stmt->execute($parameters);
        } catch (\Exception $e) {
            Message::warning($e->getMessage());
        }

        return $stmt->fetchAll();
    }

    public function skipProfile()
    {
        $this->execute('update auth_ik_users set profile = 1 where id = :id', [
            ':id' => Session::getUser()->id
        ]);
    }

    public function findCookieToken($token)
    {
        return $this->executeAndFetch("select user_id, token, expires from auth_ik_auth_tokens where token = :token", [
            ':token' => $token
        ]);
    }

    public function getUserProfileById($id)
    {
        return $this->executeAndFetch("select id, username, email, activated, type, role, profile from auth_ik_users where id = :id", [
            ':id' => $id
        ]);
    }

    public function saveQrToken($token)
    {
        $this->execute('insert into auth_ik_qr_tokens (token, expires) values (:token ,:expires)', [
            ':token' => $token,
            ':expires' => (new \DateTime('+15 minutes'))->format('Y-m-d H:i')
        ]);
    }

    public function qrTokenExists($token)
    {
        return $this->executeAndFetch('select * from auth_ik_qr_tokens where token = :token and expires > now()', [
            ':token' => $token
        ]);
    }

    public function verifyQrToken($token)
    {
        $this->execute('update auth_ik_qr_tokens set verified = 1 where token = :token', [
            ':token' => $token
        ]);
    }

    public function checkVerifiedQrToken($token)
    {
        return $this->executeAndFetch('select * from auth_ik_qr_tokens where token = :token and verified = 1', [
            ':token' => $token
        ]);
    }

    public function invalidateQrToken($token)
    {
        $this->execute('delete from auth_ik_qr_tokens where token = :token ', [
            ':token' => $token
        ]);
    }

    public function saveSmsToken($number, $token)
    {
        $this->execute('insert into auth_ik_sms_tokens (number, token, expires) values (:number, :token, :expires)', [
            ':number' => $number,
            ':token' => $token,
            ':expires' => (new \DateTime('+15 minutes'))->format('Y-m-d H:i')
        ]);
    }

    public function smsTokenExists($token)
    {
        return $this->executeAndFetch('select * from auth_ik_sms_tokens where token = :token', [
            ':token' => $token
        ]);
    }

    public function invalidateSmsToken($token)
    {
        $this->execute('delete from auth_ik_sms_tokens where token = :token', [
            ':token' => $token
        ]);
    }

    public function saveOtacCode($parameters)
    {
        $this->execute("insert into auth_ik_otac_codes (email, code) values (:email, :code)", $parameters);
    }

    public function getEmailByOtac($code)
    {
        return $this->executeAndFetch("select email from auth_ik_otac_codes where code = :code", [
            ':code' => $code
        ]);
    }

    public function createDemoAccount($params)
    {
        $this->execute("insert into auth_ik_users (username, email, password, type) values (:username, :email, :password, 'demo')", $params);
    }

    public function getUserProfileByUsername($username)
    {
        return $this->executeAndFetch("select id, username, email, activated, type, role, profile, password from auth_ik_users where username = :username", [
            ':username' => $username
        ]);
    }

    public function getUserProfileByEmail($email)
    {
        return $this->executeAndFetch("select id, username, email, activated, type, role, profile, password from auth_ik_users where email = :email", [
            ':email' => $email
        ]);
    }

    public function saveSessionToken($params)
    {
        $this->execute("insert into auth_ik_auth_tokens (user_id, token, expires) values (:user_id, :token, :expires)", $params);
    }

    public function registerUser($params)
    {
        $this->execute("insert into auth_ik_users (username, email, password) values (:username, :email, :password)", $params);
    }

    public function registerSocial($params)
    {
        $this->execute("insert into auth_ik_users (id, username, email, type) values (:id, :username, :email, :type)", [
            ':username' => $params['username'],
            ':email' => $params['email'],
            ':id' => $params['id'],
            ':type' => $params['type']
        ]);
    }

    public function saveEmailToken($params)
    {
        $this->execute("insert into auth_ik_email_tokens (user_id, verification_hash) values (:user_id, :ver_hash)", $params);
    }

    public function getUserProfileByUserId($id)
    {
        return $this->executeAndFetch("select * from auth_ik_users_profiles where user_id = :user_id", [
            ':user_id' => $id
        ]);
    }

    public function savePasswordResetToken($params)
    {
        $this->execute('insert into auth_ik_password_resets (user_id, reset_hash) values (:user_id, :reset_hash)', $params);
    }

    public function getUserIdFromPasswordResetToken($token)
    {
        return $this->executeAndFetch('select user_id from auth_ik_password_resets where reset_hash = :hash', [
            ':hash' => $token
        ]);
    }

    public function updateUserPassword($params)
    {
        $this->execute('update auth_ik_users set password = :pass where id = :id', $params);
    }

    public function getUserIdFromEmailToken($token)
    {
        return $this->executeAndFetch('select user_id from auth_ik_email_tokens where verification_hash = :hash', [
            ':hash' => $token
        ]);
    }

    public function activateUser($userId)
    {
        $this->execute('update auth_ik_users set activated = 1 where id = :id', [
           ':id' => $userId
        ]);
    }

    public function removeEmailToken($token)
    {
        $this->execute('delete from auth_ik_email_tokens where verification_hash = :hash', [
            ':hash' => $token
        ]);
    }

    public function createProfile($params)
    {
        $this->execute('insert into auth_ik_users_profiles (user_id, firstName, lastName, sex, country, avatar) values (:user_id, :firstName, :lastName, :sex, :country, :avatar)', $params);
    }

    public function updateProfile($array)
    {
        $this->execute('update auth_ik_users_profiles set firstName = :firstName, lastName = :lastName, sex = :sex, country = :country, avatar = :avatar where user_id = :id', $array);
    }

    public function getSocialData($id)
    {
        return $this->executeAndFetch('select * from auth_ik_socials where id = :id', [
            ':id' => $id
        ]);
    }

    public function saveSocialData($data)
    {
        $this->execute('insert into auth_ik_socials (id, type, name, email) values (:id, :type, :name, :email)', $data);
    }

    public function getUsersWithProfiles()
    {
        return $this->executeAndFetchAll('select * from auth_ik_users left join auth_ik_users_profiles on auth_ik_users.id = auth_ik_users_profiles.user_id', []);
    }

    public function upgradeAccount($email, $password)
    {
        $this->execute('update auth_ik_users set password = :password, type = 0 where email = :email', [
            ':password' => $password,
            ':email' => $email
        ]);
    }
}