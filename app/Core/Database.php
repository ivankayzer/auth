<?php
namespace IK_Authentication\Core;
/**
 * Class Database. Used to connect to the database.
 *
 * It is a modified version (by https://github.com/panique/huge) of Jon Raphaelson's answer on StackOverflow:
 * http://stackoverflow.com/questions/130878/global-or-singleton-for-database-connection
 */

use IK_Authentication\Core\Config;
use IK_Authentication\Views\Message;

class Database
{
    private static $factory;
    private $database;

    /**
     * Connects to the database with credentials given in the Config file.
     */
    public static function getFactory()
    {
        if(!self::$factory) {
            self::$factory = new Database();
        }

        return self::$factory;
    }

    /**
     * @return \PDO connection to db object.
     * @throws \Exception
     */
    public function getConnection()
    {
        if(!$this->database){

            try{
                $options = [
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_WARNING
                ];

                $this->database = new \PDO(
                    Config::get('db.type') . ':host=' . Config::get('db.host') . ';dbname=' .
                    Config::get('db.name') . ';port=' . Config::get('db.port'), Config::get('db.username'),
                    Config::get('db.password'), $options
                );

            } catch (\PDOException $e){
                Message::error('Failed connect to database. Error: ' . $e->getCode());
                Redirect::redirect();
            }

        }

        return $this->database;
    }
}