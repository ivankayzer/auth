<?php

namespace IK_Authentication\Core;

use Gregwar\Captcha\CaptchaBuilder;

class Captcha
{
    /**
     * Check if captcha entered correctly
     *
     * @param $captcha . Input from form
     * @return bool
     */
    public static function check($captcha)
    {
        $local = $_SESSION['captcha'];
        unset($_SESSION['captcha']);
        return $local === strtolower($captcha);
    }

    /**
     * Regenerate the captcha
     */
    public static function regenerate()
    {
        if (Tokenizer::checkToken()) echo self::generate();
    }

    /**
     * Generates captcha and return its url
     *
     * @return string. Captcha image url
     */
    public static function generate()
    {
        $builder = new CaptchaBuilder;
        $builder->build();
        $_SESSION['captcha'] = $builder->getPhrase();
        return $builder->inline();
    }
}