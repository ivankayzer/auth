<?php

namespace IK_Authentication\Core;

use IK_Authentication\Security\HashPassword;
use IK_Authentication\Security\Validate;
use IK_Authentication\Services\DatabaseService;
use IK_Authentication\Views\Message;

/**
 * Class OTAC - One-time authorization code. Creates the demo account to grant instant access to the members area. Demo account can be upgraded to normal.
 */
class OTAC
{
    protected $userEmail;
    protected $userName;
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }

    /**
     * Send otac.
     *
     * @param $id . Email to send the link
     * @throws \Exception
     */
    public function sendLink($id)
    {
        $this->userEmail = Validate::email($id);
        $this->userName = explode("@", $this->userEmail)[0];
        $otac = Tokenizer::generateRandomToken();

        $mail = new Mail;
        $mail->sendOTACcode([$this->userEmail => $this->userName], $otac);

        if (Message::hasErrors()) {
            Redirect::redirect();
        }

        $this->database->saveOtacCode([
            ':code' => $otac,
            ':email' => $this->userEmail
        ]);

        Message::success('Check your email');
        Redirect::redirect();
    }


    /**
     * Verify the code
     *
     * @param $code
     */
    public function verify($code)
    {
        $email = $this->database->getEmailByOtac($code);

        $user = $this->database->getUserProfileByEmail($email->email);

        if ($user) {
            Session::setUser($user);
            return Redirect::redirect();
        }

        if ($email) {
            return $this->createDemoAccount($email->email);
        }

        Message::error('Wrong code');
        return Redirect::redirect();
    }

    /**
     * Creates a demo account
     *
     * @param $email
     */
    public function createDemoAccount($email)
    {
        $charsToReplace = ['.', '!', ',', '<', '>'];
        $username = str_replace($charsToReplace, '', explode("@", $email)[0] . rand(1, 999));
        $password = (new HashPassword('demo'))->get();

        $cookie = Tokenizer::generateRandomToken();
        $datePlus24H = (new Auth)->convertDate(new \DateTime('now +1 day'));

        $this->database->createDemoAccount([
            ':username' => $username,
            ':email' => $email,
            ':password' => $password
        ]);

        $user = $this->database->getUserProfileByUsername($username);

        if (!$user) {
            Message::error('No user found');
            Redirect::redirect();
        }

        $this->database->saveSessionToken([
            ':user_id' => $user->id,
            ':token' => $cookie,
            ':expires' => $datePlus24H
        ]);

        setcookie('auth_ses', $cookie, time() + 24 * 3600);

        Session::setUser($user);
        Redirect::redirectToMembersArea();
    }

}