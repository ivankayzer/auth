<?php

namespace IK_Authentication\Core;

use IK_Authentication\Services\DatabaseService;
use IK_Authentication\Views\Message;

/**
 * Class Auth.
 */
class Auth
{
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }

    /**
     * Allow only authorized users to access the page.
     * If user is not authorized he will be redirected to the home page.
     */
    public static function only()
    {
        (new self)->checkCookie();
        if (!isset($_SESSION['user']['id'])) {
            Message::error('You have to login to access this page');
            Redirect::redirect();
        }
    }

    /**
     * Checks if remember cookie exists.
     * @return bool
     */
    public function checkCookie()
    {
        if (isset($_COOKIE['auth_ses']) && strlen($_COOKIE['auth_ses']) && !isset($_SESSION['user']['id'])) {
            $token = $this->database->findCookieToken($_COOKIE['auth_ses']);
            $date = $this->convertDate(new \DateTime());
            if ($token->token == $_COOKIE['auth_ses'] && $token->expires > $date) {
                $user = $this->database->getUserProfileById($token->user_id);
                if ($user != false) {
                    Session::setUser($user);
                }
            }
        }

        return true;
    }

    /**
     * Converts a DateTime object to a database format
     *
     * @param \DateTime $date
     * @return string, to put to the database
     */
    public function convertDate(\DateTime $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}