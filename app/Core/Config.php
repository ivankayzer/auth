<?php
/**
 * Class Config. Used to access /configuration.php
 */
namespace IK_Authentication\Core;

class Config
{
    protected static $config;
    protected static $default = null;

    /**
     * Get a value from file
     *
     * @param string $key value to get. (eg. 'db.name')
     * @param string $default value which will be set if $key not found in the file
     *
     * @return null|string $key or $default value
     * @throws \Exception if configuration file not found.
     */
    public static function get($key, $default = null)
    {
        if(!self::$config){
            $file = AUTH_ROOT . '/configuration.php';

            if(!file_exists($file)) throw new \Exception('Configuration file not found');

            self::$config = require $file;
        }

        self::$default = $default;
        $pieces = explode('.', $key);
        $data = self::$config;

        foreach($pieces as $piece){
            if(isset($data[$piece])){
                $data = $data[$piece];
            } else {
                $data = self::$default;
                break;
            }
        }
        return $data;
    }

    /**
     * Check if value exists in the configuration file
     *
     * @param string $key value
     *
     * @return bool value exists
     * @throws \Exception
     */
    public static function exists($key)
    {
        return self::get($key) !== self::$default;
    }

    /**
     * Get type of the project from the file. Available types: email, username, otac, Socials.
     *
     * @return string|null depending if project type is set in the file
     * @throws \Exception
     */
    public static function getAuthType()
    {
        return self::get('project.type');
    }
}