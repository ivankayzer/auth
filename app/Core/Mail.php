<?php
namespace IK_Authentication\Core;

use IK_Authentication\Views\Message;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use PHPMailer;
/**
 * Class Mail. Used to send an email
 * TODO Make emails look like real emails.
 */
class Mail
{
    protected $mail;

    /**
     * Mail constructor. Basic settings for PHPMailer object.
     * @throws \Exception
     */
    public function __construct()
    {
         $this->mail = new PHPMailer;
         $this->mail->SMTPDebug = 0;
         $this->mail->isSMTP();
         $this->mail->Host = Config::get('mail.host');
         $this->mail->SMTPAuth = true;
         $this->mail->Username = Config::get('mail.username');
         $this->mail->Password = Config::get('mail.password');
         $this->mail->SMTPSecure = Config::get('mail.encryption');
         $this->mail->Port = Config::get('mail.port');
         $this->mail->isHTML(true);

    }

    /**
     * @param array $to , recipient(-s) (eg. ['ivan.kayzer@outlook.com' => 'Ivan Kayzer']);
     * @param string $subject , subject of the email
     * @param string $body , body of the email
     * @param string $altBody , non-HTML body of the email
     * @param array $from , from whom
     */
    public function sendMail(array $to, $subject, $body, $altBody, array $from = ['me', 'Auth'])
    {
        $from[0] === 'me' ? $this->mail->setFrom(Config::get('mail.username'),
            Config::get('mail.name')) :  $this->mail->setFrom($from[0], $from[1]);

        foreach($to as $address => $name){
             $this->mail->addAddress($address, $name);
        }

        $this->mail->addReplyTo(Config::get('mail.username'), Config::get('project.name'));
        $this->mail->Subject = $subject;
        $this->mail->Body = $body;
        $this->mail->AltBody = $altBody;

        $log = new Logger('mail');
        $log->pushHandler(new StreamHandler(AUTH_ROOT . '/app/log.txt', Logger::WARNING));

         if(!$this->mail->send()){
             Message::error('Message could not be sent.');
             $log->warning("Mailer Error" . $this->mail->ErrorInfo);
         }
    }

    public function sendWelcomeEmail($to)
    {
        $this->sendMail($to, 'Welcome!', 'You have successfully registered', 'You have successfully registered');
    }

    public function sendActivationEmail($to, $hash)
    {
        if (Config::get('project.requireActivation')) {
            $url = Config::get('project.url') . '/user/verify?code=' . $hash;
            $this->sendMail($to, 'Activate your account!', 'Click <a href="' . $url . '">here</a> to activate your account', 'Click on activation link');
        }
    }

    public function sendPasswordReset($to, $hash)
    {
        $url = Config::get('project.url') . '/reset/check/' . $hash;
        $message = "Click here to reset your password: <a href='{$url}'>Reset</a>";
        $this->sendMail($to, 'Password reset', $message, $message);
    }

    public function sendOTACcode($to, $code)
    {
        $message = Config::get('project.url') . "/otac/verify/" . $code;
        $this->sendMail($to, 'Here is your OTAC code', $message, $message);
    }
}