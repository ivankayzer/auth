<?php

namespace IK_Authentication\Core;

class Redirect
{
    /**
     * Redirect to the page
     * @param string $url redirect to.
     */
    public static function redirect($url = '/')
    {
        header('Location: /auth/' . $url, true, 301);
        exit;
    }

    /**
     * Redirects to the previous page
     */
    public static function redirectBack()
    {
        self::redirect(Session::urlHistory());
    }

    /**
     * Redirects to the members area set in Config file
     */
    public static function redirectToMembersArea()
    {
        self::redirect(Config::get('project.membersArea'));
    }

    /**
     * Redirects the authorized user to the /dashboard.
     */
    public static function redirectAuthorized()
    {
        // (new Auth)->checkCookie();
        if(isset($_SESSION['user']['id'])) {
            self::redirectToMembersArea();
        }
    }
}