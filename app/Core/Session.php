<?php
namespace IK_Authentication\Core;
use IK_Authentication\Services\DatabaseService;
use IK_Authentication\Views\View;

/**
 * Class Session. Starts the session if not started. Destroys the session (e.g. for logout).
 * Checks if session exists (looks for any session_id)
 */
class Session
{
    protected $database;

    public function __construct()
    {
        $this->database = new DatabaseService();
    }

    /**
     *  Starts the session if not started.
     */
    public static function start()
    {
        if(empty(session_id())){
            session_start();
        }
    }

    /**
     *
     * @return bool indicating the existence of the session.
     */
    public static function check()
    {
        return (bool) !empty(session_id());
    }

    public static function urlHistory()
    {
        self::start();
        $previous = '/';
        if(!isset($_SESSION['urlCache'])){
            $_SESSION['urlCache'] = $_SERVER['PHP_SELF'];
        } else {
            $previous = $_SESSION['urlCache'];
            $_SESSION['urlCache'] = $_SERVER['PHP_SELF'];
        }
        return $previous;
    }

    /**
     * Destroys the session, unsets all the session variables.
     */
    public static function destroy()
    {
        self::start();
        session_unset();
        session_destroy();
    }

    /**
     * Put user data into the session
     *
     * @param object $user, user data from database
     */
    public static function setUser($user)
    {
        $user = (object) $user;
        $_SESSION['user']['id'] = $user->id;

        if(empty($user->username) && !empty($user->email)){
            $user->username = explode('@', $user->email)[0];
        }

        $_SESSION['user']['username'] = $user->username;
        $_SESSION['user']['email'] = $user->email;
        $_SESSION['user']['type'] = $user->type;
        $_SESSION['user']['activated'] = (bool) $user->activated;
        $_SESSION['user']['role'] = $user->role;
        $_SESSION['user']['profile'] = $user->profile;
    }

    /**
     * Return a user object from the session
     *
     * @return object User Object
     */
    public static function getUser()
    {
        if (isset($_SESSION['user'])) {
            return (object) $_SESSION['user'];
        }

        return self::getUserStub();
    }

    public static function getUserStub()
    {
        $stub = new \stdClass();
        $stub->password = null;
        $stub->username = null;
        $stub->email = null;
        $stub->type = null;
        $stub->role = null;
        $stub->activated = null;
        $stub->profile = null;

        return $stub;
    }


    /** Set a user profile data
     * @param array $data. Array with user profile data
     */
    public static function setProfile($data)
    {
        $_SESSION['profile']['firstName'] = $data['firstName'];
        $_SESSION['profile']['lastName'] = $data['lastName'];
        $_SESSION['profile']['country'] = $data['country'];
        $_SESSION['profile']['sex'] = $data['sex'];
        $_SESSION['profile']['avatar'] = $data['avatar'];
    }

    /** Return user's profile
     * @return object
     */
    public static function getProfile()
    {
        if (isset($_SESSION['profile'])) return (object) $_SESSION['profile'];
    }

    /** Return the user's avatar to embed into html.
     * @return string
     * @throws \Exception
     */
    public static function getProfileAvatar()
    {
        $projUrl = Config::get('project.url');

        if (isset($_SESSION['profile'])) {
            return "<img class='avatar' src='{$projUrl}/storage/avatars/" . $_SESSION['profile']['avatar'] . " '>";
        }
    }

    /**
     * Checks if user filled in or skipped the profile creation page.
     */
    public function createProfileOnFirstLogin()
    {
        if (!$this->getUser()->profile) {
            new View('profile');
            die();
        }
    }

    /**
     * Skip profile creation.
     */
    public function skipProfile()
    {
        $this->database->skipProfile();

        $_SESSION['user']['profile'] = 1;
        Redirect::redirectToMembersArea();
    }

    public function isUpgradable()
    {
        $type = $this->getUser()->type;
        return $type === 'demo';
    }

    /**
     * Checks if user is admin
     *
     * @return bool
     */
    public static function isAdmin()
    {
        return self::getUser()->role === '0';
    }
}