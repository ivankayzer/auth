<?php

namespace IK_Authentication\Core;

use IK_Authentication\Views\ViewLoader;
use \IK_Authentication\Views\View;

/**
 * Class Router.
 * TODO Make it more readable
 * Makes it possible to use pretty url's (/user/add) in the application.
 * All requests go through this router.
 * Get arguments look like this : (/user/add/1) - 1 is an argument and will be sent to the controller specified in add method.
 * Post arguments' name should match the controller's method arguments.
 * (example: for the method login($username, $password) you should send $_POST['username'], $_POST['password'])
 */
class Router
{

    private $routes = [];
    public $trim = '$/\&^';
    private $controller;
    private $controller_name;
    private $method;
    private $type;
    private $args = [];

    /**
     * Add the 'pretty url' to the router
     *
     * @param string $type Use GET or POST to send the data to the controller.
     * @param string $url Url to look for.
     * @param string $controller Controller to call.
     * @param string $method Controllers method to call.
     */
    public function add($type, $url, $controller, $method)
    {
        $url = trim($url, $this->trim);
        $this->routes[] = array(
            'type' => $type,
            'url' => $url,
            'offset' => count(explode('/', $url)),
            'controller' => $controller,
            'method' => $method
        );
    }

    /**
     * Gets current url and compares with registered paths
     *
     * @return View with 404 error if Controller not found.
     */
    public function start()
    {
        $url = isset($_REQUEST['auth']) ? trim($_REQUEST['auth'], $this->trim) : '/';
        $url = explode('/', $url);
        foreach ($this->routes as $route) {
            $route['url'] = explode('/', $route['url']);
            if (count($url) >= $route['offset']) {

                $match = array_slice($url, 0, $route['offset']);

                if ($match == $route['url']) {
                    $this->type = $route['type'];
                    $this->controller_name = $route['controller'];
                    $this->method = $route['method'];
                    $this->args = array_slice($url, $route['offset'], count($url));
                }
            }
        }

        if (!empty($this->controller_name)) {
            $this->controller_name = $this->generateClassName($this->controller_name);
            $this->controller = new $this->controller_name;
            $args = [];

            if ($this->type == 'GET') {
                $args = $this->args;
            }

            if ($this->type == 'POST') {
                $r = new \ReflectionMethod($this->controller, $this->method);
                $parameters = $r->getParameters();
                foreach ($parameters as $parameter) {
                    if (!empty($_POST[$parameter->getName()])) {
                        $args[] = $_POST[$parameter->getName()];
                    }
                }
            }

            return call_user_func_array(array($this->controller, $this->method), $args);

        }
        return new View('404');
    }

    public function generateClassName($class)
    {
        $names = [
            "Core:Config", "Core:Auth", "Core:Database", "Core:Mail", "Core:OTAC", "Core:Router", "Core:Session", "Core:Tokenizer",
            "Models:User", "Security:Hash", "Security:Validate", "Providers:Facebook", "Providers:Google", "Providers:AuthenticationProvider",
            "Providers:Twitter", "Providers:Qr", "Providers:Sms", "Views:Message", "Views:View", "Views:ViewLoader", "Services:DatabaseService",
            "Services:QrService", "Services:SmsService", "Controllers:AuthController", "Controllers:Controller", "Controllers:ProfileController",
            "Controllers:ResetController", "Controllers:UserController"
        ];

        foreach ($names as $name) {
            $path = explode(":", $name);
            if ($class === $path[1]) {
                return 'IK_Authentication\\' . $path[0] . '\\' . $class;
            }
        }
    }

}