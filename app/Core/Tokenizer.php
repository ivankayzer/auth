<?php

namespace IK_Authentication\Core;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Tokenizer
{
    /**
     * Generates the token and puts it to the session.
     */
    public static function generateCsrfToken()
    {
        Session::start();
        if(empty($_SESSION['token']))
            $_SESSION['token'] = Tokenizer::generateRandomToken();
    }

    /**
     * Generate random token.
     *
     * @return string token
     */
    public static function generateRandomToken()
    {
        return bin2hex(random_bytes(32));
    }

    public static function generateToken($length = null)
    {
        if ($length) {
            return substr(bin2hex(random_bytes(5)), 0, 5);
        }

        return self::generateRandomToken();
    }

    /**
     * Return the current token.
     *
     * @return string|null token from the session or null
     */
    public static function getToken()
    {
        if(!empty($_SESSION['token'])){
            return $_SESSION['token'];
        }
    }

    /**
     * Compare the session token with the token in request.
     *
     * @return bool indicating if token matches the Session value.
     */
    public static function checkToken()
    {
        if(empty($_POST['token']) || !hash_equals($_POST['token'], $_SESSION['token'])){
            $log = new Logger('auth');
            $log->pushHandler(new StreamHandler(AUTH_ROOT . '/app/log.txt', Logger::ERROR));
            $log->error('CSRF TOKENS DO NOT MATCH');
            return false;
        }

        return true;
    }

    /**
     * Remove the token from the session
     */
    public static function removeToken()
    {
        if(!empty($_SESSION['token'])){
            unset($_SESSION['token']);
        }
    }
}