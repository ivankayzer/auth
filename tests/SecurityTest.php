<?php

namespace Tests;

use IK_Authentication\Security\HashPassword;
use IK_Authentication\Security\Validate;
use PHPUnit\Framework\TestCase;

class SecurityTest extends TestCase
{
    public function testShortUsername()
    {
        $username = "usr";
        $this->assertEquals(false, Validate::username($username));
    }

    public function testLongUsername()
    {
        $username = "usernameIsTooLoooooooooooooong";
        $this->assertEquals(false, Validate::username($username));
    }

    public function testWrongUsername()
    {
        $username1 = "11123USERfdsNAME";
        $username2 = "username..";
        $username3 = "usdsfdd!!";
        $username4 = 'user@gd$';
        $username5 = '<script>alert(1)</script>';

        $this->assertEquals(false, Validate::username($username1));
        $this->assertEquals(false, Validate::username($username2));
        $this->assertEquals(false, Validate::username($username3));
        $this->assertEquals(false, Validate::username($username4));
        $this->assertEquals(false, Validate::username($username5));

    }

    public function testRightUsername()
    {
        $username = "administrator";
        $this->assertEquals($username, Validate::username($username));
    }

    public function testWrongPassword()
    {
        $pass1 = "<password!";
        $pass2 = "ELHS?@DD>";

        $this->assertEquals(false, Validate::password($pass1));
        $this->assertEquals(false, Validate::password($pass2));
    }

    public function testRightPassword()
    {
        $pass1 = 'GoodPassword123';
        $pass2 = 'Also_Good_Password!!';
        $pass3 = 'Another.Good.123.Password!';
        $pass4 = 'demo';

        $this->assertEquals($pass1, Validate::password($pass1));
        $this->assertEquals($pass2, Validate::password($pass2));
        $this->assertEquals($pass3, Validate::password($pass3));
        $this->assertEquals($pass4, Validate::password($pass4));

    }

    public function testWrongEmail()
    {
        $email1 = "test@mail";
        $email2 = "test@mail.";
        $email3 = "tesdtgkgf";

        $this->assertEquals(false, Validate::email($email1));
        $this->assertEquals(false, Validate::email($email2));
        $this->assertEquals(false, Validate::email($email3));
    }

    public function testRightEmail()
    {
        $email1 = "ivan.kayzer@outlook.com";
        $email2 = "ivan.xbee@gmail.com";
        $email3 = "ikayz3r@gmail.com";

        $this->assertEquals($email1, Validate::email($email1));
        $this->assertEquals($email2, Validate::email($email2));
        $this->assertEquals($email3, Validate::email($email3));

    }
}