<?php $path = '/auth/'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Send sms</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo $path; ?>views/style.css">
</head>
<body>

<h2 class="text-center reset">Read QR</h2>

<div class="form-reset" style="text-align: center;">
    <img src='<?= (new IK_Authentication\Services\QrService)->generate(); ?>' />

    <div class="foot-lnk">
        <a href="<?php echo $path; ?>">Go Back</a>
    </div>
</div>

<script type="text/javascript">
    setInterval(function () {
        console.log('Checking QR code');
    }, 3000);
</script>

</body>
</html>