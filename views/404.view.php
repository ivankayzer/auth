<?php $path = '/auth/'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>404 - Not found</title>
    <?php require 'links.html' ?>
</head>
<body>


<div id="e404container">
    <div class="e404">
        <h1>404</h1>
        <p>page not found</p>
    </div>
    <a href="<?php echo $path; ?>">Back to home page</a>
</div>

<!-- TODO Move to errors/ folder and throw them as errors not just views.-->
<?php require 'scripts.html' ?>
</body>
</html>