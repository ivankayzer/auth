<?php $path = '/auth/'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome! - Home page</title>
    <?php use IK_Authentication\Core\Config;
    use IK_Authentication\Core\Tokenizer;
    use IK_Authentication\Views\Message;

    require 'links.html'; ?>
</head>
<body>
<div class="form-wrap">
    <div class="messages"><?php Message::display(); ?></div>
    <div class="form-html">
        <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
        <input id="tab-2" type="radio" name="tab" class="sign-up">
        <label for="tab-2" class="tab">
            <?php if ($type != 'otac'): ?>
            Sign Up
            <?php endif; ?>
        </label>
        <div class="form">

            <?php if($type != 'otac'): ?>

            <form class="sign-in-htm" action="login" method="post">
                <div class="group">
                    <label for="id" class="label"><?php echo $type; ?></label>
                    <input id="id" name="loginData[id]" type="text" class="input">
                </div>
                <div class="group">
                    <label for="password" class="label">Password</label>
                    <input id="password" name="loginData[password]" type="password" class="input" data-type="password">
                </div>
                <div class="group">
                    <input id="remember" type="checkbox" class="check" name="loginData[remember]" checked>
                    <label for="check"> Keep me Signed in</label>
                </div>
                <input type="hidden" name="token" value="<?php echo Tokenizer::getToken(); ?>">
                <div class="group">
                    <input type="submit" class="button" value="Sign In">
                </div>
                <div class="socials">
                    <?php
                    if(Config::get('project.enableSocials')){
                        echo $twitter;
                        echo $facebook;
                        echo $google;
                        echo $sms;
                    }
                    ?>
                </div>
                <div class="hr"></div>
                <div class="foot-lnk">
                    <a href="<?php echo $path; ?>reset">Forgot Password?</a>
                </div>
            </form>

            <?php else: ?>

            <form class="sign-in-htm" action="<?php echo $path; ?>otac" method="post">
                <div class="group">
                    <label for="id" class="label"><?php echo $type; ?></label>
                    <input id="id" name="id" type="text" class="input">
                </div>
                <input type="hidden" name="token" value="<?php echo Tokenizer::getToken(); ?>">
                <div class="group">
                    <input type="submit" class="button" value="Send code">
                </div>
            </form>

            <?php endif; ?>


            <?php if($type != 'otac'): ?>
            <form class="sign-up-htm" action="<?php echo $path; ?>user/add" method="post">
                <div class="group">
                    <label for="user" class="label">Username</label>
                    <input id="user" name="regData[username]" type="text" class="input">
                </div>
                <div class="group">
                    <label for="password" class="label">Password</label>
                    <input id="password" type="password" name="regData[password]" class="input" data-type="password">
                </div>
                <div class="group">
                    <label for="email" class="label">Email Address</label>
                    <input id="email" type="text" name="regData[email]" class="input">
                </div>
                <div class="group">
                    <label for="captcha" class="label">Captcha</label>
                    <img src="<?php echo $captcha; ?>" alt="captcha" class="img-captcha">
<!--                    <i class="fa fa-refresh refresh" aria-hidden="true"></i>-->
                    <input id="captcha" type="text" name="regData[captcha]" class="input" value="">
                </div>
                <input type="hidden" name="token" value="<?php echo Tokenizer::getToken(); ?>">
                <div class="group">
                    <input type="submit" class="button" value="Sign Up">
                </div>
                <div class="hr"></div>
                <div class="foot-lnk">
                    <label for="tab-1">Already Member?</label>
                </div>
            </form>
            <?php endif; ?>
        </div>
</div>
</div>
<?php require 'scripts.html' ?>
</body>
</html>