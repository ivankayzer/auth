<?php $path = '/auth/';
use IK_Authentication\Core\Tokenizer;
use IK_Authentication\Views\Message; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Send sms</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo $path; ?>views/style.css">
</head>
<body>
<div class="messages"><?php Message::display(); ?></div>
<h2 class="text-center reset">Check sms code</h2>

<div class="form-reset">
    <div class="form">
        <form action="<?php echo $path; ?>sms/verify" class="form-htm" method="post">
            <div class="group">
                <label for="number" class="label">Number</label>
                <input type="text" name="number" class="input" value="<?= htmlentities($_GET['phone']) ?>"
                       readonly="readonly">
            </div>
            <div class="group">
                <label for="code" class="label">Sms code</label>
                <input type="text" name="code" class="input">
            </div>
            <input type="hidden" name="token" value="<?= Tokenizer::getToken(); ?>">
            <div class="group">
                <input type="submit" class="button" value="Verify">
            </div>
            <div class="hr"></div>
            <div class="foot-lnk">
                <a href="<?php echo $path; ?>">Go Back</a>
            </div>
        </form>
    </div>
</div>
</body>
</html>