<?php $path = '/auth/'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <?php
    require 'links.html'; ?>
</head>
<body>
<div class="container">
    <?php var_dump($users); ?>
    <?php if ($users): ?>
        <h1>Users list</h1>
        <table class="table table-striped">
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo $user->id; ?></td>
                    <td><?php echo $user->username; ?></td>
                    <td><?php echo $user->email; ?></td>
                    <td><?php echo $user->role === '0' ? 'Admin' : 'User'; ?></td>
                    <td><?php echo $user->firstName; ?></td>
                    <td><?php echo $user->lastName; ?></td>
                    <td><?php echo $user->sex; ?></td>
                    <td><?php echo $user->country; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>
<?php require 'scripts.html'; ?>
</body>
</html>