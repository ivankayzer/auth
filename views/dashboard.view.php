<?php $path = '/auth/'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
    <?php use IK_Authentication\Core\Auth;
    use IK_Authentication\Core\Session;
    use IK_Authentication\Core\Tokenizer;
    use IK_Authentication\Views\Message;

    require 'links.html'; ?>
</head>
<body>
    <div class="container">
        <h1 class="text-center">Dashboard</h1>
        <?php $user = Session::getUser(); ?>
        <?php if((new Session)->isUpgradable()):  ?>
            <form action="<?php echo $path; ?>user/upgrade" method="get">
            <button type="submit" class="btn btn-primary btn-lg btn-block">Upgrade account</button>
            </form>
        <?php endif; ?>
        <div class="space"></div>
        <table class="table text">
            <tr>
                <td>id</td>
                <td><?php echo $user->id; ?></td>
            </tr>
            <tr>
            <td>username</td>
            <td><?php echo $user->username; ?></td>
            </tr>
            <tr>
                <td>email</td>
                <td><?php echo $user->email; ?></td>
            </tr>
        </table>
        <hr>
        <div class="space"></div>
        <div class="clearfix"></div>
        <?php if((new Session)->isAdmin()) : ?>
            <a href="<?php echo $path; ?>admin"><button class="btn btn-primary pull-right">Admin area</button></a>
        <?php endif; ?>
        <div class="space"></div>
        <div class="clearfix"></div>
        <h2 class="text-center">Profile data</h2>
        <hr>
        <div class="space"></div>
        <div class="col-md-12">
        <?php $profile = Session::getProfile(); ?>
        <?php if ($profile): ?>
        <form action="<?php echo $path; ?>profile/update/" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
            <div class="form-group">
                <input type="text" class="form-control" name="profileData[firstName]" value="<?php echo $profile->firstName; ?>">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="profileData[lastName]" value="<?php echo $profile->lastName; ?>">
            </div>
            <div class="form-group">
                <select class="form-control" name="profileData[sex]">
                    <option selected="selected" value="<?php echo $profile->sex; ?>" disabled><?php echo $profile->sex == '0' ? 'Male' : 'Female'; ?></option>
                    <option value="0">Male</option>
                    <option value="1">Female</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="profileData[country]" value="<?php echo $profile->country; ?>">
            </div>
            </div>
            <div class="col-md-4 pull-right">
                <div class="form-group">
                    <?php echo Session::getProfileAvatar(); ?>
                    <div class="small-space"></div>
                    <input type="file" name="avatar" id="avatar">
                </div>
            </div>
            <input type="hidden" name="token" value="<?php echo Tokenizer::getToken(); ?>">
            <div class="clearfix"></div>
            <input type="submit" value="Save" class="btn-primary btn">
        </form>
        <?php else: ?>
            <p>You have no profile</p>
            <a href="<?php echo $path; ?>profile/create"><button class="btn btn-primary">Create one</button></a>
        <?php endif; ?>
        </div>

        <div class="clearfix"></div>
        <div class="space"></div>
        <hr>
        <form action="<?php echo $path; ?>logout" method="post">
            <input type="hidden" name="token" value="<?php echo Tokenizer::getToken(); ?>">
            <input type="submit" value="Logout" class="btn btn-danger btn-lg pull-right">
        </form>

        <div class="messages">
            <?php Message::display(); ?>
        </div>
    </div>

    <?php require 'scripts.html'; ?>
</body>
</html>