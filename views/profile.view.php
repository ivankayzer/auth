<?php $path = '/auth/'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo $path; ?>/views/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="messages"><?php use IK_Authentication\Core\Session;
    use IK_Authentication\Views\Message;

    Message::display(); ?></div>
<div class="form-profile">
<div class="form">
    <form class="form-htm" action="<?php echo $path; ?>profile/save" method="post" enctype="multipart/form-data">
        <div class="group">
            <label for="firstName" class="label">First name</label>
            <input id="firstName" name="data[firstName]" type="text" class="input">
        </div>
        <div class="group">
            <label for="lastName" class="label">Last name</label>
            <input id="lastName" name="data[lastName]" type="text" class="input">
        </div>
        <div class="group">
            <label for="sex" class="label">Sex</label>
            <input id="sex" name="data[sex]" type="radio" checked value="Male"> Male
            <input id="sex" name="data[sex]" type="radio" value="Female"> Female
        </div>
        <div class="group">
            <label for="email" class="label">Email</label>
            <input id="email" name="data[email]" type="text" class="input" disabled value="<?php echo Session::getUser()->email; ?>">
            <span class="fa fa-lock lockedField"></span>
        </div>
        <div class="group">
            <label for="country" class="label">Country</label>
            <input id="country" name="data[country]" type="text" class="input">
        </div>
        <div class="group">
            <label for="avatar" class="label">Avatar</label>
            <input id="avatar" name="avatar" type="file" class="input">
        </div>
        <div class="image-preview">
            <img id="image">
        </div>
        <input type="hidden" name="token" value="">
        <div class="group">
            <input type="submit" class="button" value="Save and continue">
        </div>
        <div class="hr"></div>
        <div class="foot-lnk">
            <a href="<?php echo $path; ?>profile/skip">Skip this step</a>
        </div>
    </form>
</div>
</div>
<script>
    if (document.getElementById("avatar")) {
        document.getElementById("avatar").onchange = function () {
            var reader = new FileReader();

            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("image").src = e.target.result;
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        };
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</body>
</html>