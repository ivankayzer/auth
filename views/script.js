// $('.register').on('click', function(){ display($(this)); });

// $('.login').on('click', function(){ display($(this)); });

// function display(el) {
//     el.prop('disabled', true).removeClass('btn-default').addClass('btn-primary');
//     el.siblings('button').prop('disabled', false).removeClass('btn-primary').addClass('btn-default');

//     var form = $('#form-' + el[0].classList[1]);

//     form.siblings('form').fadeOut(function(){
//         form.fadeIn();
//     });
// }

$('.refresh').on('click', function(){
    $.ajax({
        url: '/captcha/new',
        method: 'post',
        data: {
            token : $('[name=token]').val()
        },
        success: function(data){
            $('.img-captcha').attr('src', data);
        }
    });
});

if (document.getElementById("avatar")) {
    document.getElementById("avatar").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
}

