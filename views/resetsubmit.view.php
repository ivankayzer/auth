<?php $path = '/auth/'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo $path; ?>views/style.css">
</head>
<body>


<h2 class="text-center reset">Reset password</h2>

<div class="form-reset">
    <div class="form">
        <form action="<?php echo $path; ?>reset/save" class="form-htm" method="post">
            <div class="group">
                <label for="password" class="label">New password</label>
                <input type="password" name="password" class="input">
            </div>
            <div class="group">
                <label for="confirmation" class="label">Confirm password</label>
                <input type="password" name="confirmation" class="input">
            </div>
            <input type="hidden" value="<?php
            use IK_Authentication\Core\Tokenizer;

            echo $hash; ?>" name="hash">
            <input type="hidden" name="token" value="<?php echo Tokenizer::getToken(); ?>">
            <div class="group">
                <input type="submit" class="button" value="Reset password">
            </div>
            <div class="hr"></div>
            <div class="foot-lnk">
                <a href="<?php echo $path; ?>">Go Back</a>
            </div>
        </form>
    </div>
</div>
</body>
</html>