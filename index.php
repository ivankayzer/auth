<?php
require_once 'vendor/autoload.php';

use IK_Authentication\Core\Session;
use IK_Authentication\Core\Router;
use IK_Authentication\Core\Config;
use IK_Authentication\Core\Tokenizer;

Session::start();
Tokenizer::generateCsrfToken();

$router = new Router();

$router->add('POST', '/login', 'AuthController', 'login');
$router->add('POST', '/logout', 'AuthController', 'logout');
$router->add('GET', '/admin', 'AuthController', 'adminArea');
$router->add('POST', '/user/add', 'UserController', 'register');
$router->add('GET', '/user/upgrade', 'UserController', 'upgrade');
$router->add('GET', '/user/verify', 'UserController', 'activate');
$router->add('POST', '/user/upgrade', 'UserController', 'upgrade');
$router->add('GET', '/reset', 'ViewLoader', 'reset');
$router->add('POST', '/reset/save', 'ResetController', 'resetPassword');
$router->add('POST', '/reset/send', 'ResetController', 'createResetHash');
$router->add('POST', '/profile/update', 'ProfileController', 'updateProfile');
$router->add('GET', '/profile/create', 'ProfileController', 'recreateProfile');
$router->add('POST', '/profile/save', 'ProfileController', 'createProfile');


$router->add('GET', '/', 'ViewLoader', 'index');
$router->add('GET', '/sms', 'ViewLoader', 'sms');
$router->add('GET', '/send', 'ViewLoader', 'mail');
$router->add('GET', '/test', 'ViewLoader', 'testView');
$router->add('GET', '/smsCheck', 'ViewLoader', 'smsCheck');
$router->add('GET', '/dashboard', 'ViewLoader', 'dashboard');
$router->add('GET', '/reset/check', 'ViewLoader', 'resetSubmit'); // use to send in email /reset/check/$hash

$router->add('POST', '/captcha/new', 'Captcha', 'regenerate');

$router->add('POST', '/otac', 'OTAC', 'sendLink');
$router->add('GET', '/otac/verify', 'OTAC', 'verify');

$router->add('POST', '/profile/skip', 'Session', 'skipProfile');

$router->add('GET', '/facebook', 'Facebook', 'callback');

$router->add('GET', '/google', 'Google', 'callback');

$router->add('GET', '/twitter', 'Twitter', 'callback');
$router->add('GET', 'twitter/profile', 'Twitter', 'profile');

$router->add('GET', '/sms/send', 'Sms', 'callback');
$router->add('POST', '/sms/verify', 'SmsService', 'check');

//$router->add('GET', '/qr', 'ViewLoader', 'qr');
//$router->add('POST', '/qr/verify', 'QrService', 'check');

$router->start();