<?php

use IK_Authentication\Core\Session;

define('AUTH_ROOT', dirname(__FILE__));

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

//session to keep previous page

Session::urlHistory();