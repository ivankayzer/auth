drop table if exists auth_ik_users;
drop table if exists auth_ik_auth_tokens;
drop table if exists auth_ik_email_tokens;
drop table if exists auth_ik_password_resets;
drop table if exists auth_ik_otac_codes;
drop table if exists auth_ik_users_profiles;

create table auth_ik_users(
  id int(11) auto_increment primary key,
  username varchar(40) not null unique,
  email varchar(60) not null unique,
  password char(255),
  type varchar(20) default '0',
  role int(11) default 1,
  activated tinyint(1) default 1,
  profile tinyint(1) default 0
);

INSERT into auth_ik_users (username, email, password, type, role, profile) values ('admin', 'ivan.kayzer@outlook.com', '$2y$10$N3ZbjUWfA/l3GYfKtzFBYO5K4gT4oRyjkxkENBqcVVeDFHYFIdHRS', 0, 0, 0); -- admin

create table auth_ik_auth_tokens(
  id int(11) auto_increment primary key,
  user_id int(11) not null,
  token char(255),
  expires datetime,
  foreign key (user_id) references auth_ik_users(id) on delete cascade
);

create table auth_ik_email_tokens(
  id int(11) auto_increment primary key,
  user_id int(11) not null,
  verification_hash char(255),
  foreign key (user_id) references auth_ik_users(id) on delete cascade
);

create table auth_ik_password_resets(
  id int(11) auto_increment primary key,
  user_id int(11) not null,
  reset_hash char(255),
  foreign key (user_id) references auth_ik_users(id) on delete cascade
);

create table auth_ik_otac_codes(
  id int(11) auto_increment primary key,
  email varchar(60) not null,
  code char(255)
);

create table auth_ik_users_profiles(
  user_id int(11) not null,
  firstName varchar(40),
  lastName varchar(60),
  sex tinyint(1),
  country varchar(30),
  avatar varchar(255),
  foreign key (user_id) references auth_ik_users(id) on delete cascade
);

create table auth_ik_socials(
  id char(255) not null primary key,
  type varchar(20),
  name varchar(40),
  email varchar(60)
);

create table auth_ik_sms_tokens(
  id int(11) auto_increment primary key,
  number char(13) not null,
  token char(255) not null,
  expires datetime
);

/* create table auth_ik_qr_tokens(
  id int(11) auto_increment primary key,
  token char(255) not null,
  verified tinyint(1) default 0
  expires datetime
);
*/